package io.bensalem;

import static org.junit.Assert.*;

import org.junit.Test;

public class MathUtilsTest {

	@Test
	public void testAddition() {
		MathUtils mathUtils = new MathUtils();
		assertEquals(4, mathUtils.addition(2, 2));
	}

	@Test
	public void testMultiplication() {
		MathUtils mathUtils = new MathUtils();
		assertEquals(4, mathUtils.multiplication(2, 2));
	}

	@Test
	public void testDevision() {
		MathUtils mathUtils = new MathUtils();
		assertEquals(1, mathUtils.devision(2, 2));

	}

	@Test
	public void testSubtraction() {
		MathUtils mathUtils = new MathUtils();
		assertEquals(0, mathUtils.subtraction(2, 2));

	}

}
